**Localhost setup**
Sonar:
1. download and unzip SonarQube in to opt/ folder (or wherever preferred)
2. add global <settings> to settings.xml located in .m2 folder/create one
3. run sonar from the command line such as: `opt/sonarqube/bin/macosx-universal-64/sonar.sh console`
4. in the browser go to `http://localhost:9000` (default login admin admin) and 
    follow the on screen instruction to generate a token
5. from the root directory of the project run 
    `mvn sonar:sonar`
    or if running the first time
    `mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=the-generated-token`
6. go to `http://localhost:9000/projects` to see the report
7. to stop the sonar running in the console press ctrl + c

DataStore Emulator local:
1. install gcp emulator on local machine by running the command in terminal :
    `gcloud components install gcd-emulator`
2. start the emulator - run in the command line 
    `gcloud beta emulators datastore start`
3. Run the following command to set the environment variables, so that your all will use the local datastore 
   running instead the Google Cloud Datastore server - 
   `gcloud beta emulators datastore env-init`

