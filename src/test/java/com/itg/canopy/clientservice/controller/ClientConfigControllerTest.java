package com.itg.canopy.clientservice.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.itg.canopy.clientservice.entity.Client;
import com.itg.canopy.clientservice.exception.ClientException;
import com.itg.canopy.clientservice.service.ClientErrorMessageConfig;
import com.itg.canopy.clientservice.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientConfigController.class)
public class ClientConfigControllerTest {
    @MockBean
    private ClientService clientService;
    @MockBean
    private ClientErrorMessageConfig clientErrorMessageConfig;
    @Autowired
    private MockMvc mockMvc;
    private Client client;
    private String clientJsonPath = "/client/client.json";

    @Before
    public void setup() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        this.client = objectMapper.readValue(getClass().getResourceAsStream((clientJsonPath)), Client.class);
    }

    //POST Method
    @Test
    public void saveClientRecordSuccess() throws Exception {
        doNothing().when(clientService).saveClientRecord(any());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/clients")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void saveClientBadRequestExceptionTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/clients")
                .content(asJsonString("{}"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    //PUT Method
    @Test
    public void updateClientSuccessTest() throws Exception {
        doNothing().when(clientService).updateClientRecord(any());

        mockMvc.perform(MockMvcRequestBuilders
                .put("/v1/clients")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateClientNotFoundTest() throws Exception {
        doThrow(ClientException.class).when(clientService).updateClientRecord(any());

        mockMvc.perform(MockMvcRequestBuilders
                .put("/v1/clients")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    //GET Method
    @Test
    public void getClientRecordSuccessTest() throws Exception {
        //given
        Long clientId = 1L;

        //when
        when(clientService.getClientConfig(clientId)).thenReturn(any());

        //then
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/clients/" + clientId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getClientRecordNotFoundTest() throws Exception {
        //given
        Long clientId = 2L;

        //when
        when(clientService.getClientConfig(clientId)).thenThrow(ClientException.class);

        //then
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/clients/" + clientId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}