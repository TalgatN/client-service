/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.clientservice.service;


import com.itg.canopy.clientservice.entity.Client;
import com.itg.canopy.clientservice.exception.ClientException;
import com.itg.canopy.clientservice.repository.ClientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClientServiceTest {
    @MockBean
    private ClientRepository clientRepository;
    @Autowired
    private ClientService clientService;
    private Client client;

    @Before
    public void setup() {
        this.client = new Client();
        client.setId(123L);
        client.setName("TestCase");
        client.setStatus("active");
        client.setPaymentGatewayApiKey("test");
    }

    @Test
    public void getClientConfigSuccess() throws Exception {
        when(clientRepository.findById(123L)).thenReturn(Optional.of(client));
        Client client1 = clientService.getClientConfig(123L);

        assertThat(client1.getId(), is(123L));
        assertThat(client1.getName(), is("TestCase"));
        assertThat(client1.getStatus(), is("active"));
        assertThat(client1.getPaymentGatewayApiKey(), is("test"));
    }

    @Test(expected = ClientException.class)
    public void getClientConfigNotFoundException() throws Exception {
        when(clientRepository.findById(345L)).thenReturn(Optional.empty());
        Client client1 = clientService.getClientConfig(345L);

        assertNull(client1);
    }
}