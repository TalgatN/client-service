package com.itg.canopy.clientservice.service;

import com.itg.canopy.clientservice.entity.Client;
import com.itg.canopy.clientservice.exception.ClientException;
import com.itg.canopy.clientservice.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final ClientErrorMessageConfig errorMessageConfig;

    public ClientServiceImpl(ClientRepository clientRepository, ClientErrorMessageConfig errorMessageConfig) {
        this.clientRepository = clientRepository;
        this.errorMessageConfig = errorMessageConfig;
    }

    @Override
    public Client getClientConfig(Long id) throws ClientException {
        log.info("reading client object with the ID: " + id);

        return clientRepository.findById(id).orElseThrow(() -> new ClientException(errorMessageConfig.getClientNotFoundMessage()));
    }

    @Override
    public void saveClientRecord(Client client) {
        log.info("Saving client object with the uuid: " + client.getUuid());
        clientRepository.save(client);
    }

    @Override
    public void updateClientRecord(Client client) throws ClientException {
        if (!clientRepository.existsById(client.getId()))
            throw new ClientException(errorMessageConfig.getClientNotFoundMessage());

        log.info("Updating client object with the ID: " + client.getId());
        clientRepository.save(client);
    }
}
