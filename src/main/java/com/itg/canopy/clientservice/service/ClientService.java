package com.itg.canopy.clientservice.service;


import com.itg.canopy.clientservice.entity.Client;
import com.itg.canopy.clientservice.exception.ClientException;

public interface ClientService {

    Client getClientConfig(Long id) throws Exception;
    void saveClientRecord(Client client);
    void updateClientRecord(Client client) throws Exception;
}
