package com.itg.canopy.clientservice.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Getter
public class ClientErrorMessageConfig {
    @Value("${client.error.dto.validation}")
    private String dtoValidationErrorMessage;
    @Value("${client.error.not.found}")
    private String clientNotFoundMessage;

}
