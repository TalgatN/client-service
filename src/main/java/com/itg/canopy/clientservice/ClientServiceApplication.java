/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.clientservice;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class ClientServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientServiceApplication.class, args);
	}


	@Bean
	@Profile("local")
	public Datastore cloudDatastoreService(){
		return DatastoreOptions.newBuilder().setHost("http://localhost:8081").setProjectId("itg-dev-customersvc").build().getService();
	}

}
