package com.itg.canopy.clientservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
@Slf4j
public class ClientExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ClientException.class})
    public void handleBadRequest(ClientException exception, HttpServletResponse response) throws IOException {
        log.error("Exception: ", exception);
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({Exception.class})
    public void handleInternalServerErrorRequest(Exception exception,  HttpServletResponse response) throws IOException {
        log.error("Exception: ", exception);
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }


}
