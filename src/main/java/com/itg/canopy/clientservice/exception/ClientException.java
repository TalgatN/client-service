package com.itg.canopy.clientservice.exception;

public class ClientException extends Exception {
    public ClientException(String exception) {
        super(exception);
    }
}
