package com.itg.canopy.clientservice.repository;

import com.itg.canopy.clientservice.entity.Client;
import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;

public interface ClientRepository extends DatastoreRepository<Client, Long> {

}
