package com.itg.canopy.clientservice.controller;


import com.itg.canopy.clientservice.entity.Client;
import com.itg.canopy.clientservice.exception.ClientException;
import com.itg.canopy.clientservice.service.ClientErrorMessageConfig;
import com.itg.canopy.clientservice.service.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/clients")
public class ClientConfigController {
    private final ClientService clientService;
    private final ClientErrorMessageConfig errorMessageConfig;

    public ClientConfigController(ClientService clientService, ClientErrorMessageConfig errorMessageConfig) {
        this.clientService = clientService;
        this.errorMessageConfig = errorMessageConfig;
    }

    @GetMapping(value ="/{id}")
    public ResponseEntity<Client> findByID(@PathVariable Long id) throws Exception{

        Client client = clientService.getClientConfig(id);

        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @PostMapping(value = "")
    public ResponseEntity createClientRecord(@Valid @RequestBody Client client, BindingResult result) throws Exception {
        if (result.hasErrors())
            throw new ClientException(errorMessageConfig.getDtoValidationErrorMessage());

        clientService.saveClientRecord(client);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping(value = "")
    public ResponseEntity updateTransaction(@Valid @RequestBody Client client, BindingResult result) throws Exception {
        if (result.hasErrors())
            throw new ClientException(errorMessageConfig.getDtoValidationErrorMessage());

        clientService.updateClientRecord(client);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
