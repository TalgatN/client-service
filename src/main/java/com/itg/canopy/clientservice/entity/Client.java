package com.itg.canopy.clientservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Entity(name = "Client")
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter@Setter
public class Client {
    @Id
    private Long id;
    private UUID uuid = UUID.randomUUID();
    @NotEmpty
    private String name;
    @NotEmpty
    private String status;
    @NotEmpty
    private String paymentGatewayApiKey;
}
